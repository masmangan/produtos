package incubadora.produtos;

public class Produto {

	private String nome;
	private double preco;
	
	public Produto(String nome, double preco) {
		super();
		this.nome = nome;
		this.preco = preco;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getNome() {
		return nome;
	}

	@Override
	public String toString() {
		return String.format("Produto [nome=%s, preco=%s, toString()=%s]", nome, preco, super.toString());
	}

	
}
