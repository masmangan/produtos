package incubadora.produtos;

import java.util.GregorianCalendar;

public class Exemplos {
	public static void main(String[] args) {
		System.out.println("Exemplos");
		Produto p1 = new Produto("Teclado Logitech", 50.0);
		
		GregorianCalendar cal = new GregorianCalendar(2020, 1, 1);
		
		/* Polimorfismo */
		Produto p2 = new ProdutoPerecivel("Cloroquina", 1.0, cal.getTime());
	
		ProdutoPerecivel p3 = new ProdutoPerecivel("Abacaxi", 3.4, cal.getTime());

		Produto[] ps = {p1, p2, p3};
		ProdutoPerecivel[] pps = {p3};

		if (p2 instanceof ProdutoPerecivel) {
			ProdutoPerecivel coringa = (ProdutoPerecivel)p2;
		}
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);

	}
}
