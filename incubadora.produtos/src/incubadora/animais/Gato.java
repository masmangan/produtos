package incubadora.animais;

public class Gato extends Animal{

	public Gato(String nome) {
		super(nome);
	}

	@Override
	public void falar() {
		System.out.printf("O gato %s falou Miau!%n", nome);
	}

	@Override
	public String toString() {
		return String.format("Gato [nome=%s]", nome);
	}

}
